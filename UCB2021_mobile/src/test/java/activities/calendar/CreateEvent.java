package activities.calendar;

import controlAppium.Button;
import controlAppium.TextBox;
import org.openqa.selenium.By;

public class CreateEvent {
    public Button anniversaryButton= new Button(By.id("com.xiaomi.calendar:id/tab_image_anniversary"));
    public TextBox titleTextBox = new TextBox(By.id("com.xiaomi.calendar:id/title"));
    public Button saveButton= new Button(By.id("com.xiaomi.calendar:id/action_done"));

}

