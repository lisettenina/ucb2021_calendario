package activities.calendar;

import controlAppium.Button;
import controlAppium.Label;
import org.openqa.selenium.By;

public class MainScreen {

    public Button acceptButton = new Button(By.id("android:id/button1"));
    public Button addEventButton = new Button(By.id("com.xiaomi.calendar:id/new_event"));
    public Label nameEventLabel = new Label(By.id("com.xiaomi.calendar:id/primary"));

    public MainScreen(){}
}
