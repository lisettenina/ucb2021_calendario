package cleanTest;
import activities.calendar.CreateEvent;
import activities.calendar.MainScreen;
import activities.calendar.UpdateEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import session.Session;
import java.net.MalformedURLException;

public class EventCalendar {
    private MainScreen mainScreen= new MainScreen();
    private CreateEvent createEvent= new CreateEvent();
    private UpdateEvent updateEvent =new UpdateEvent();

    @Test
    public void verifyCreateEvent() throws MalformedURLException {
        String title="TEST_ANNIVERSARY";
        mainScreen.acceptButton.click();
        mainScreen.addEventButton.click();
        createEvent.anniversaryButton.click();
        createEvent.titleTextBox.setValue(title);
        createEvent.saveButton.click();

        Assertions.assertEquals(title,mainScreen.nameEventLabel.getText(),"ERROR! event was not created");
    }

    @Test
    public void verifyUpdateEvent() throws MalformedURLException {

        String titleUpdate = "event_UPDATE";
        mainScreen.acceptButton.click();
        mainScreen.nameEventLabel.click();
        updateEvent.editButton.click();
        createEvent.titleTextBox.setValue(titleUpdate);
        createEvent.saveButton.click();

        Assertions.assertEquals(titleUpdate, mainScreen.nameEventLabel.getText(), "ERROR! title was not update");
    }

    @AfterEach
    public void close() throws MalformedURLException {
        Session.getInstance().closeSession();
    }

}
